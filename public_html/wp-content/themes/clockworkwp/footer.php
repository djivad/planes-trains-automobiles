<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ClockworkWP
 */
$footer_bgcolor = get_theme_mod( 'footer_bgcolor' );
?>

	<footer id="colophon" class="site-footer">
		<?php
			global $svg;
			$shape = get_theme_mod( 'svg_shape' );
			if( $shape <> 'none' ) :
				echo sprintf( '<div class="shapes"><div class="shape top">%1$s</div></div>', $svg[$shape] );
			endif; ?>
	<aside class="fatfooter">
		<?php if ( is_active_sidebar( 'footer-widget-one' ) ) : ?>
		<div class="footer-widget-one widget-area">
			<?php dynamic_sidebar( 'footer-widget-one' ); ?>
		</div><!-- .footer-widget-one -->
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-widget-one' ) && is_active_sidebar( 'footer-widget-two' ) ) : ?>
		<div class="gap"></div>
		<?php endif; ?>
		<?php if ( is_active_sidebar( 'footer-widget-two' ) ) : ?>
		<div class="footer-widget-two widget-area">
			<?php dynamic_sidebar( 'footer-widget-two' ); ?>
		</div><!-- .footer-widget-two -->
		<?php endif; ?>
	</aside><!-- #fatfooter -->
		<div class="site-info">
			<div class="copyright">
					<?php
					/* translators: %s: Copyright Year, 2: Copyright Owner */
					printf( wp_kses_post( get_theme_mod( 'footer_copyright' ) ) );
					?>
				 | Read the <a href="<?php echo esc_url( __( '/hashtag/intro/', 'clockworkwp' ) ); ?>">Intro articles</a>
			</div>
			<div class="gap"></div>
			<div class="credit">
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( '%1$s theme by %2$s', 'clockworkwp' ), 'ClockworkWP', 'David Riviera' );
					?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
