/**
 * Copyright (c) 2021
 *
 * Primary Javascript functionality
 *
 * @summary main javascript file
 * @author David Riviera <d@driviera.com>
 *
 * Created at     : 2021-06-11 13:41:00
 * Last modified  : 2021-06-21 16:45:08
 */

// Vanilla Javascript version to deprecate jQuery
window.addEventListener('load', function () {

	/* Find the .hamburger */
	var hamburger = document.querySelector("#hamburger");
	/* Click event handler */
	hamburger.addEventListener("click", function() {
		/* Toggles class "is-active" on .hamburger */
		hamburger.classList.toggle("is-active");
		/* Finds the mobile menu */
		var mobile = document.querySelector(".mobile-navigation");
		/* Changes class "active" on .mobile-menu to show/hide */
		mobile.classList.toggle("active");
	});
	
	
}, false);