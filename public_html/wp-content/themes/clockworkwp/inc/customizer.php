<?php
/**
 * ClockworkWP Theme Customizer
 *
 * @package ClockworkWP
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

/**
 * Customize Settings
 */

function clockworkwp_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'clockworkwp_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'clockworkwp_customize_partial_blogdescription',
			)
		);
	}
	
	/**
	 * Alpha Color Picker
	 */
	require get_template_directory() . '/admin/alpha-color-picker/alpha-color-picker.php';
	
	
	/* Create Header Section */
	$wp_customize->add_section(
		'shape_options',
		array(
			'title'     => 'Shapes',
			'priority'  => 50,
		)
	);
	
	
	/* Create Footer Section */
	$wp_customize->add_section(
		'footer_options',
		array(
			'title'     => 'Footer',
			'priority'  => 115,
		)
	);

	/* Header SVG Shape
	 * - sawtooth
	 * - curve
	 * - point
	 * - none
	 */
	$wp_customize->add_setting( 'svg_shape', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => 'curve',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'svg_shape', array(
			'type' => 'radio',
			'label' => __( 'Shape Image', 'clockworkwp' ),
			'section' => 'shape_options',
			'choices' => array(
				'curve' => __( 'Curve' ),
				'point' => __( 'Point' ),
				'sawtooth' => __( 'Sawtooth' ),
				'none' => __( 'None' ),
			  ),
		)
	  )
	);

	/* Shape Height (px) */
	$wp_customize->add_setting( 'svg_shape_height', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => 142,
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => 'absint',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'svg_shape_height', array(
			'type' => 'range',
			'label' => __( 'Shape Image Height', 'clockworkwp' ),
			'section' => 'shape_options',
			'input_attrs' => array(
				'min' => 0,
				'max' => 100,
				'step' => 1,
			),
		)
	  )
	);
		
	
	

	/* Footer .copyright Text */
	$wp_customize->add_setting( 'footer_copyright', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '© ' . get_bloginfo( 'name' ),
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'footer_copyright', array(
			'type' => 'textarea',
			'label' => __( 'Footer Copyright Text', 'clockworkwp' ),
			'section' => 'footer_options',
		)
	  )
	);

	
	
	/* Text Color */
	$wp_customize->add_setting( 'body_text_color', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#444444',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'body_text_color', array(
			'label' => __( 'Text Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);


	
	
	/* Link Color */
	$wp_customize->add_setting( 'body_link_color', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#0000cc',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'body_link_color', array(
			'label' => __( 'Link Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);
	
	
	
	
	
	/* Link Hover Color */
	$wp_customize->add_setting( 'body_link_hover_color', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#00cc00',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'body_link_hover_color', array(
			'label' => __( 'Link Hover Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);

	/* Header Gradient Angle (deg) */
	$wp_customize->add_setting( 'header_gradient_angle', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => 142,
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => 'absint',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Control( $wp_customize, 'header_gradient_angle', array(
			'type' => 'range',
			'label' => __( 'Header Gradient Angle', 'clockworkwp' ),
			'section' => 'colors',
			'input_attrs' => array(
				'min' => 0,
				'max' => 360,
				'step' => 1,
			),
		)
	  )
	);
		
	
	/* Header Gradient Color 1 */
	$wp_customize->add_setting( 'header_gradient_one', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => 'rgba( 127, 0, 0, 0.5 )',
		'transport' => 'refresh', // or postMessage
		//'sanitize_callback' => '',
		//'sanitize_js_callback' => '', // Basically to_json.
		//'input_attrs' => array(
    		//'class' => 'color-picker',
			//),
		)
	  );
	$wp_customize->add_control(
		new Customize_Alpha_Color_Control(
            $wp_customize,
            'header_gradient_one',
            array(
                'label'        => __( 'Header Gradient One', 'clockworkwp' ),
                'section'      => 'colors',
                'settings'     => 'header_gradient_one',
                'show_opacity' => true, // Optional.
                'palette'      => array(
                    'rgb(150, 50, 220)',
                    'rgba(50,50,50,0.8)',
                    'rgba( 255, 255, 255, 0.2 )',
                    '#00CC99' // Mix of color types = no problem
                )
            )
        )
		//new WP_Customize_Color_Control( $wp_customize, 'header_gradient_one', array(
		//	'label' => __( 'Header Gradient Two', 'clockworkwp' ),
		//	'section' => 'colors',
		//)
	  //)
	);


	/* Header Gradient Color 2 */
	$wp_customize->add_setting( 'header_gradient_two', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => 'rgba( 255, 127, 54, 0.5 )',
		'transport' => 'refresh', // or postMessage
		//'sanitize_callback' => '',
		//'sanitize_js_callback' => '', // Basically to_json.
		//'input_attrs' => array(
    		//'class' => 'color-picker',
			//),
		)
	  );
	$wp_customize->add_control(
		new Customize_Alpha_Color_Control(
            $wp_customize,
            'header_gradient_two',
            array(
                'label'        => __( 'Header Gradient Two', 'clockworkwp' ),
                'section'      => 'colors',
                'settings'     => 'header_gradient_two',
                'show_opacity' => true, // Optional.
                'palette'      => array(
                    'rgb(150, 50, 220)',
                    'rgba(50,50,50,0.8)',
                    'rgba( 255, 255, 255, 0.2 )',
                    '#00CC99' // Mix of color types = no problem
                )
            )
        )
	);
	
	/* Footer Background Color onto .site-footer */
	$wp_customize->add_setting( 'footer_bgcolor', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#e1e1e1',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'footer_bgcolor', array(
			'label' => __( 'Footer Background Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);
	/* Footer Text Color onto .site-footer */
	$wp_customize->add_setting( 'footer_textcolor', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#333',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'footer_textcolor', array(
			'label' => __( 'Footer Text Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);

	/* Footer Link Color onto .site-footer */
	$wp_customize->add_setting( 'footer_link_color', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#990000',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'footer_link_color', array(
			'label' => __( 'Footer Link Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);

	


	/* Footer Link Hover Color onto .site-footer */
	$wp_customize->add_setting( 'footer_link_hover_color', array(
		'type' => 'theme_mod', // or 'option'
		'capability' => 'edit_theme_options',
		'theme_supports' => '', // Rarely needed.
		'default' => '#ff0000',
		'transport' => 'refresh', // or postMessage
		'sanitize_callback' => '',
		'sanitize_js_callback' => '', // Basically to_json.
		)
	  );
	$wp_customize->add_control(
		new WP_Customize_Color_Control( $wp_customize, 'footer_link_hover_color', array(
			'label' => __( 'Footer Link Hover Color', 'clockworkwp' ),
			'section' => 'colors',
		)
	  )
	);
}
add_action( 'customize_register', 'clockworkwp_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function clockworkwp_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function clockworkwp_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function clockworkwp_customize_preview_js() {
	wp_enqueue_script( 'clockworkwp-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'clockworkwp_customize_preview_js' );


/**
 * Get Customize Options and apply to <style> block
 */
add_action( 'wp_head', function() {
	$customize_settings = array(
		array(
			'element' => 'a',
			'style' => array (
				'color' => get_theme_mod( 'body_link_color' ),
			),
		),
		array(
			'element' => 'a:hover',
			'style' => array (
				'color' => get_theme_mod( 'body_link_hover_color' ),
			),
		),
		array(
			'element' => '.site-footer',
			'style' => array(
					'background' => get_theme_mod( 'footer_bgcolor' ),
					'color' => get_theme_mod( 'footer_textcolor' ),
				)
			),
		array(
			'element' => '.site-footer a',
			'style' => array (
				'color' => get_theme_mod( 'footer_link_color' ),
			),
		),
		array(
			'element' => '.site-footer a:hover',
			'style' => array (
				'color' => get_theme_mod( 'footer_link_hover_color' ),
			),
		),
		array(
			'element' => 'body, button, input, select, optgroup, textarea',
			'style' => array (
				'color' => get_theme_mod( 'body_text_color' ),
			),
		),
		array(
			'element' => '.shapes .shape svg',
			'style' => array (
				'fill' => '#' . get_theme_mod( 'background_color' ),
				'height' => get_theme_mod( 'svg_shape_height' ) . 'px',
			),
		),
		array(
			'element' => '.fatfooter',
			'style' => array (
				'padding-top' => get_theme_mod( 'svg_shape_height' ) . 'px',
			),
		),
		array(
			'element' => '.site-header',
			'style' => array (
				'padding-bottom' => get_theme_mod( 'svg_shape_height' ) . 'px',
			),
		),
	);
	_e( '<style id="clockwork-inline-css">' );
		foreach( $customize_settings as $elements ) {
			_e( $elements['element'] . ' {' );
				foreach( $elements['style'] as $key=>$setting ) {
					_e( $key . ': ' . $setting . '; ' );
				}
			_e( '} ' );
		}
	_e( '</style>' );
	
}, 20 );

/*
 * SVG images for header .shape and footer .shape
 */	global $svg;
	$svg['curve'] = '<svg class="theshape" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 270" preserveAspectRatio="none"><path d="M1920,0c-254.7,110.2-581,264.9-960,265C580.5,265.1,255,110.3,0,0v270h1920V0z"/><path d="M1742.6,423.6"/></svg>';
	$svg['point'] = '<svg class="theshape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 135" preserveAspectRatio="none"><polyline  points="960 120.899 1920 0 1920 135 0 135 0 0"/><path d="M960,202.5" transform="translate(0 -135)"/><path d="M341.176,135" transform="translate(0 -135)"/></svg>';
	$svg['sawtooth'] = '<svg class="theshape" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 135" preserveAspectRatio="none"><polygon class="e6a94725-9153-4fd3-a885-1fe29e67f740" points="1920 135 0 135 0 0 135.294 67.5 341.176 0 568.235 103.529 874.118 0 1165.882 85.882 1305.882 0 1633.203 77.152 1920 0 1920 135"/></svg>';



