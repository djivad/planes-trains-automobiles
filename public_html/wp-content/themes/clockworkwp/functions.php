<?php
/**
 * ClockworkWP functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ClockworkWP
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '0.0.1' );
}

if ( ! function_exists( 'clockworkwp_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function clockworkwp_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on ClockworkWP, use a find and replace
		 * to change 'clockworkwp' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'clockworkwp', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'clockworkwp' ),
				'mobile-1' => esc_html__( 'Mobile', 'clockworkwp' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'clockworkwp_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
					'default-position-x' => 'center',
					'default-position-y' => 'center',
					'default-repeat'     => 'no-repeat',
					'default-size'	=> 'cover',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'clockworkwp_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function clockworkwp_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'clockworkwp_content_width', 640 );
}
add_action( 'after_setup_theme', 'clockworkwp_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function clockworkwp_widgets_init() {
	
	// Sidebar Widget
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'clockworkwp' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'clockworkwp' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	
	// First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Footer Widget One', 'clockworkwp' ),
        'id' => 'footer-widget-one',
        'description' => __( 'The first footer widget area', 'clockworkwp' ),
        'before_widget' => '<section id="%1$s" class="widget widget-container %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Footer Widget Two', 'clockworkwp' ),
        'id' => 'footer-widget-two',
        'description' => __( 'The second footer widget area', 'clockworkwp' ),
        'before_widget' => '<section id="%1$s" class="widget widget-container %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
}
add_action( 'widgets_init', 'clockworkwp_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function clockworkwp_scripts() {
	wp_enqueue_style( 'clockworkwp-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_enqueue_style( 'clockworkwp-hamburger', get_stylesheet_directory_uri() . '/css/hamburgers.min.css', array(), _S_VERSION );
	wp_style_add_data( 'clockworkwp-style', 'rtl', 'replace' );

	//wp_enqueue_script( 'clockworkwp-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	//wp_enqueue_script( 'clockworkwp-jquery', get_template_directory_uri() . '/js/jquery-2.2.0.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'clockworkwp-scripts', get_template_directory_uri() . '/js/scripts.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
 	 * Alpha Color Picker for Customize
 	 */
	//wp_enqueue_script ( 'alpha-color-picker-script', get_stylesheet_directory_uri() . '/admin/alpha-color-picker/alpha-color-picker.js', array( 'jquery' ), _S_VERSION, true );
	//wp_enqueue_style( 'alpha-color-picker-style', get_stylesheet_directory_uri() . '/admin/alpha-color-picker/alpha-color-picker.css', array(), _S_VERSION );

}
add_action( 'wp_enqueue_scripts', 'clockworkwp_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Hide post excerpts from home page and archive pages
 */
 function clockworkwp_excerpt_more( $more ) {
     return '';
 }
 add_filter( 'excerpt_more', 'clockworkwp_excerpt_more' );

/**
 * Globally disable comments
 */
add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;
    
    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

/**
 * Custom image sizes
 */
add_image_size( 'square-small', 300, 300, true );
add_image_size( 'square-medium', 600, 600, true );
add_image_size( 'square-large', 1200, 1200, true );
add_image_size( 'blog-banner', 1200, 400, true );

add_filter( 'image_size_names_choose', function( $sizes ) {
    return array_merge( $sizes, array(
        'square-small' => __( 'Square Small' ),
        'square-medium' => __( 'Square Medium' ),
		'square-large' => __( 'Square Large' ),
		'blog-banner' => __( 'Blog Banner' ),
    ) );
} );

/**
 * Enqueue Fonts CSS
 */
add_action('wp_enqueue_scripts', function(){
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/css/fonts.css' );
}, 11);

/**
 * Enqueue Mobile CSS
 */
add_action('wp_enqueue_scripts', function(){
    wp_enqueue_style( 'mobile', get_template_directory_uri() . '/css/mobile.css', 'clockworkwp-style' );
}, 99);

/**
 * Enqueque Adboe Fonts style sheet for Almaq
 * font-family: almaq-refined, sans-serif;
 * font-family: almaq-rough, sans-serif;
 */
 add_action('wp_enqueue_scripts', function(){
    wp_enqueue_style( 'typekit', 'https://use.typekit.net/bpe4sty.css', 'clockworkwp-style' );
}, 1);

/**
 * Disables smart quotes site-wide
 */
add_filter( 'run_wptexturize', '__return_false' );


