<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ClockworkWP
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'clockworkwp' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="header-container">
			<div class="site-branding header-logo">
				<?php
				the_custom_logo();
				?>	
			</div><!-- .header-logo -->
			<div class="site-branding header-title">
				<?php
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				endif;
				$clockworkwp_description = get_bloginfo( 'description', 'display' );
				if ( $clockworkwp_description || is_customize_preview() ) :
					?>
					<h2 class="site-description"><?php echo $clockworkwp_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></h2>
				<?php endif; ?>
					
			</div><!-- .header-title -->
			<div class="header-nav">
				<nav id="site-navigation" class="main-navigation">
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->
			</div><!-- .header-nav -->
		</div><!-- .header-container -->
		<?php
			global $svg;
			$shape = get_theme_mod( 'svg_shape' );
			if( $shape <> 'none' ) :
				echo sprintf( '<div class="shapes"><div class="shape bottom">%1$s</div></div>', $svg[$shape] );
			endif; ?>
		<nav id="mobile-navigation" class="mobile-navigation">
						<?php
						wp_nav_menu(
							array(
								'theme_location' => 'mobile-1',
								'menu_id'        => 'mobile-menu',
							)
						);
						?>
		</nav><!-- #mobile navigation -->
		<div class="hamburgers">
			<button id="hamburger" class="hamburger hamburger--spin" type="button" aria-label="Menu" aria-controls="mobile-navigation">
			  <span class="hamburger-box">
				<span class="hamburger-inner"></span>
			  </span>
			</button>
		</div><!-- .hamburgers -->
	</header><!-- .site-header -->