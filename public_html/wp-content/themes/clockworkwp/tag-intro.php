<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ClockworkWP
 */

get_header();
?>
<div class="page-container">
<div class="page-box">
	<main id="primary" class="site-main">
		<?php
		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => '3',
			'order'			=> 'ASC',
			'orderby'		=> 'date',			
		);

		$query = new WP_Query( $args );
		?>
		<?php if ( $query->have_posts() ) : ?>

			<header class="page-header">
				<?php
				//the_archive_title( '<h1 class="page-title">', '</h1>' );
				//the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while (  $query->have_posts() ) :
				 $query->the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->
</div><!-- .page-box -->
<div class="page-sidebar">
<?php
get_sidebar();
?>
</div><!-- .page-sidebar -->
</div><!-- .page-container -->
<?php
get_footer();