<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ClockworkWP
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="col-2-layout">
		<div>
		<?php clockworkwp_post_thumbnail( 'square-small' ); ?>
		</div>
		<div>
			<?php if( is_singular() ) : ?>
				<header class="entry-header">
				<a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php esc_attr( the_title_attribute() ); ?>"><?php the_title( '<h2 class="entry-title">', '</h2>' ); ?></a>
					<div class="entry-meta">
					<?php
						echo sprintf( 'Published: <time datetime="%1$s" itemprop="datePublished">%2$s</time>', get_the_date('c'), get_the_date( 'Y-m-d' ) );
						$cats = get_the_category($id);
						( count($cats) > 1 ) ? $label = 'Categories' : $label = 'Category' ;
						$catout = $label . ': ';
						foreach ( $cats as $cat ):
							$catout .= sprintf(' <a href="%1$s">%2$s</a>,', get_category_link($cat->cat_ID), $cat->name );
						endforeach;
						echo sprintf( '<div class="categories">%1$s</div>', substr($catout, 0, -1) );
						the_tags( '<div class="tags">Tags: ', ', ', '</div>' );
					?>
					</div>
				</header><!-- .entry-header -->
			<?php else : ?>
				<header class="entry-header">
				<h3 class="entry-titlexxx"><a href="<?php echo esc_url( get_permalink() ); ?>" title="<?php esc_attr( the_title_attribute() ); ?>"><?php the_title(); ?></a></h3>
					<div class="entry-meta">
					<?php
						$cats = get_the_category($id);
						$catout = '';
						foreach ( $cats as $cat ):
							$catout .= sprintf(' <a href="%1$s">%2$s</a>,', get_category_link($cat->cat_ID), $cat->name );
						endforeach;
						echo sprintf( '<div class="categories">%1$s</div>', substr($catout, 0, -1) );
						echo sprintf( '<time datetime="%1$s" itemprop="datePublished">%2$s</time>', get_the_date('c'), get_the_date( 'Y-m-d' ) );
					?>
					</div>
				</header><!-- .entry-header -->
			<?php endif; ?>
		</div>
	</div>
		<?php
		if( is_singular() ) : ?>
		<div class="entry-content">
		<?php
			the_content();

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'clockworkwp' ),
					'after'  => '</div>',
				)
			); ?>
		</div><!-- .entry-content -->	
		<?php endif; ?>
	

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'clockworkwp' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
