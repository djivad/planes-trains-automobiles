<?php
/**
 * Template part for displaying travel logs
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ClockworkWP
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	
	
<div class="col-2-layout">
	<div>
		<?php clockworkwp_post_thumbnail( 'square-small' ); ?>
	</div>
	<div>
		<header class="entry-header">
			<?php
			if ( is_singular() ) :
				the_title( '<h2 class="entry-title">', '</h2>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
				?>
				<div class="entry-meta">
					<?php
						$meta = get_post_meta( get_the_ID() );
						if( isset($meta['Dates'][0]) ) :
							echo sprintf( '<div class="dates-traveled">%1$s</div>', $meta['Dates'][0] );
						endif;
					?>
				</div><!-- .entry-meta -->
		</header><!-- .entry-header -->
	</div>
</div>
	<?php if ( is_singular() ) :
		?>
		<div class="entry-content">
			<?php
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'clockworkwp' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				)
			);

			wp_link_pages(
				array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'clockworkwp' ),
					'after'  => '</div>',
				)
			);
			?>
		</div><!-- .entry-content -->
	<?php endif; ?>
	<footer class="entry-footer">
		<?php clockworkwp_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
