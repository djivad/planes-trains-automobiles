<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ClockworkWP
 */

get_header();
?>
<div class="page-container">
<div class="page-box">
	<main id="primary" class="site-main">

		<?php
		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => '3',
			
		);

		$query = new WP_Query( $args );
		
		
		if ( $query->have_posts() ) :

			if ( is_home() && ! is_front_page() ) :
				?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
				<?php
			endif;

			/* Start the Loop */
			while ( $query->have_posts() ) :
				$query->the_post();

				/*
				 * Include the Post-Type-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Type name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_type() );
			endwhile;
			if( is_singular() ) :
				the_posts_navigation();
			endif;

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		 
		/* Restore original Post Data */
		wp_reset_postdata();
		?>

	</main><!-- #main -->
</div><!-- .page-box -->
<div class="page-sidebar">
<?php
get_sidebar();
?>
</div><!-- .page-sidebar -->
</div><!-- .page-container -->
<?php
get_footer();
