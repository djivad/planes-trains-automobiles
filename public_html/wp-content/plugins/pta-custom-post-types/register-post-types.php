<?php
/**
 * PTA Custom Post Types
 *
 * @package           PTAPluginPackage
 * @author            David Riviera
 * @copyright         2021 David Riviera
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       PTA Custom Post Types
 * Plugin URI:        https://www.driviera.com/
 * Description:       Registers Custom Post Types for "Planes, Trains, and Automobiles"
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            David Riviera
 * Author URI:        https://www.driviera.com/
 * Text Domain:       pta-customm-post-types
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

/*
 * Add Custom Post Type: Travel Log
 */
function pta_custom_post_type() {
    register_post_type( 'travel-log',
        array(
            'labels'      => array(
                'name'          => __( 'Travel Logs', 'textdomain' ),
                'singular_name' => __( 'Travel Log', 'textdomain' ),
            ),
            'public'      => true,
            'has_archive' => true,
			'show_in_rest' => true,
			'supports' => array( 'title', 'custom-fields', 'editor', 'thumbnail' ),
            'rewrite'     => array( 'slug' => 'travel-log' ), // my custom slug
        )
    );
}
add_action('init', 'pta_custom_post_type');



