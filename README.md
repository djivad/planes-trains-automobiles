# Planes, Trains, and Automobiles is a demo site for the ClockworkWP theme.

## About

~~~
Theme Name: ClockworkWP
Theme URI: https://www.driviera.com
Author: David Riviera
Author URI: https://www.driviera.com
Description: A Project for Clockwork
Version: 0.0.1
Tested up to: 5.7.2
Requires PHP: 7.4
License: GNU General Public License v2 or later
License URI: https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
Text Domain: clockworkwp
Tags: custom-background, custom-logo, custom-menu, custom-header, featured-images, threaded-comments, translation-ready
~~~

## Starter Theme

The ClockworkWP theme is based on [Underscores](https://underscores.me/).

## Server Environment

The demo site runs on a LAMP stack consisting of Debian 10 Linux server (L) with Apache 2.4.38 (A), MariaDB 10.3.29 (M), and PHP 8.0.7 (P).

## Git

The repository is stored on BitBucket at [https://bitbucket.org/djivad/planes-trains-automobiles/](https://bitbucket.org/djivad/planes-trains-automobiles/).

The workflow is from the *sandbox* server **git push** to the BitBucket repository, then **git pull** from BitBucket to the *demo*/*production* server.

## Installation

1. Create the database in MariaDB: **create database clockworkwp;**
2. Clone the repository: **git clone https://bitbucket.org/djivad/planes-trains-automobiles/**
3. The folder *planes-trains-automobiles/* contains a subfolder *public_html/* which should be the DocumentRoot in the Apache VirtualHost
4. You may need to change the owner of the directory. Use the command **chmod -R www-data:www-data planes-trains-automobiles/** (or if you've renamed the folder, use the name you changed it to)
5. The *wp-config.php* file is ignored by Git
6. The *cache/* directory is ignored by Git
7. The Wordfence wflog/ directory is ignored by Git
8. Go through the first WordPress setup step ***only***. This writes the *wp-config.php* file with database credentials
9. Edit the *wp-config.php* file to add two lines: **define( 'WP_HOME', 'https://your_domain_name' );** and **define( 'WP_SITEURL', 'https://your_domain_name' );**
10. In MariaDB, use the commands **use clockworkwp;** then **source clockworkwp.sql;** (you must be in the same directory as *clockworkwp.sql*

## Database

The database name is *clockworkwp*. After a **git pull** use the command chain **drop database clockworkwp; create database clockworkwp; use clockworkwp; source clockworkwp.sql;** to refresh the database (***Do this only if you have not made content changes to your site--You will lose your changes***)

## WordPress Environment

The current version is WordPress 5.7.2.

## WordPress Plugins

1. Better Search Replace
2. Bulk Media Register
3. GenerateBlocks
4. Filter Category Posts, a plugin to add the [filtercategoryposts catid="#"] shortcode
5. PTA Custom Post Types, a custom plugin to add the Travel Log post type
6. Regenerate Thumbnails
7. The SEO Framework
8. W3 Total Cache
9. Wordfence Security

## Features

1. Custom post type: Travel Log, a simple method to notate trips
2. Filter Category Posts: a shortcode defined in the Filter Category Posts plugin. Use: [filtercategoryposts catid="#"]
3. Post categories include:
	* Modes of Transportation
		* Planes
		* Trains
		* Automobiles

## Dependencies

This website uses libraries and code from the following:

1. [Underscores](https://underscores.me/)
2. [Hamburgers by Jonathan Suh](https://jonsuh.com/hamburgers/)
	* jQuery 2.2.0 is included with Hamburgers
3. [WPTRT color-alpha Control](https://github.com/WPTT/control-color-alpha) *in progress*
4. [Filter Category Posts](https://bitbucket.org/djivad/filter-category-posts/)

## To-dos

1. Fine-tune gaps, spacing, and alignment of elements
2. Enhance and tidyify all CSS
3. **COMPLETE** Migrate [categoryposts] shortcode to its own plugin file
4. Tidyify functions.php
5. Improve database update process
6. **COMPLETE** Add Hamburger overlay menu for mobile
7. Add two color pickers with alpha to Customize for header gradient
8. Fix shrinking post thumbnail images inside flex
